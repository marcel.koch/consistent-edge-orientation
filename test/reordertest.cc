// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/classname.hh>
#include <dune/grid/io/file/gmshreader.hh>
#include <dune/grid/io/file/gmshwriter.hh>
#include <dune/grid/io/file/dgfparser.hh>
#ifdef HAVE_DUNE_UGGRID
#include <dune/grid/uggrid.hh>
#endif
#ifdef HAVE_DUNE_ALUGRID
#include <dune/alugrid/grid.hh>
#include <dune/alugrid/dgf.hh>
#endif

#include "dune/consistent-edge-orientation/utilities.hh"
#include "dune/consistent-edge-orientation/createconsistentgrid.hh"
#include "dune/consistent-edge-orientation/paralleledges.hh"
#include "mesh/torusgrid.hh"


template<typename GridType>
struct TestTraits{
  using Grid = GridType;
  using GV = typename Grid::LeafGridView;
  constexpr static int dim = Grid::dimension;
};

template<typename Grid, typename FuncEachEdge>
void executeForEachEdge(std::shared_ptr<Grid> grid, const FuncEachEdge &funcEachEdge) {
  constexpr int dim = Grid::dimension;
  using GV = typename Grid::LeafGridView;
  const auto& gv = grid->leafGridView();
  const auto& idxSet = gv.indexSet();

  Dune::EdgeMap<bool, GV> visited(gv, false);

  for(const auto& e: Dune::elements(gv)){
    for (std::size_t i = 0; i < e.subEntities(dim - 1); ++i) {
      Dune::EdgeWrapper<GV> edgeWrapper(e, i, idxSet.subIndex(e, i, dim - 1));
      if(!visited[edgeWrapper]){
        visited[edgeWrapper] = true;

        funcEachEdge(edgeWrapper);
      }
    }
  }
}


template<typename Grid>
struct TestEdgeToAdjacentElementsMap{
  using Traits = TestTraits<Grid>;

  void operator()(std::shared_ptr<Grid> grid) const{
    const auto& gv = grid->leafGridView();
    const auto& idxSet = gv.indexSet();

    auto adjacentElements = Dune::computeEdgeToAdjacentElementsMap(gv);

    executeForEachEdge(grid,
                       [&adjacentElements, &gv, &idxSet] (const Dune::EdgeWrapper<typename Traits::GV> &edgeWrapper) {
      if(adjacentElements[edgeWrapper].size() == 0)
        DUNE_THROW(Dune::InvalidStateException, "Edge has no adjacent elements");
      if(adjacentElements[edgeWrapper].size() > (std::size_t) gv.size(0))
        DUNE_THROW(Dune::InvalidStateException, "Edge has more adjacent elements than elements in the grid view");

      const auto globalIndex = adjacentElements[edgeWrapper][0].globalIndex;
      for (const auto &adjacentWrapper: adjacentElements[edgeWrapper])
        if(globalIndex != adjacentWrapper.globalIndex)
          DUNE_THROW(Dune::InvalidStateException, "Edges in adjacent elements are not the same");
    });
  }
};

template<typename Grid>
struct TestConsistentOrientationAfterFlip{
  using Traits = TestTraits<Grid>;

  void operator()(std::shared_ptr<Grid> grid) const{
    const auto& gv = grid->leafGridView();
    const auto& idxSet = gv.indexSet();

    auto globalOrientation = Dune::computeConsistentEdgeOrientation(gv);

    executeForEachEdge(grid,
                       [&globalOrientation, &gv, &idxSet] (const Dune::EdgeWrapper<typename Traits::GV> &edgeWrapper) {
      const auto axis = edgeWrapper.axis();
      if (axisNeedsFlip(gv, edgeWrapper.element, axis, globalOrientation)) {
        if(edgeOrientation(gv, edgeWrapper) == globalOrientation[edgeWrapper])
          DUNE_THROW(Dune::InvalidStateException, "Wrong orientation after flip");
      } else {
        if(edgeOrientation(gv, edgeWrapper) != globalOrientation[edgeWrapper])
          DUNE_THROW(Dune::InvalidStateException, "Wrong orientation after flip");
      }
    });
  }
};

template<typename Grid>
struct TestGridConsistentAfterReorder{
  using Traits = TestTraits<Grid>;

  void operator()(std::shared_ptr<Grid> grid) const{
    std::shared_ptr<Grid> newGrid(Dune::createConsistentGrid(grid));

    const auto& gv = newGrid->leafGridView();
    if(!Dune::hasConsistentOrientation(gv))
      DUNE_THROW(Dune::InvalidStateException, "Elements with shared edge do not agree on the orientation of the edge");
  }
};

template<typename Grid>
struct TestGridIntersectionTypeAfterReorder{
  using Traits = TestTraits<Grid>;

  std::array<std::size_t, 4> count_intersection_type(const typename Traits::GV& gv) const{
    std::array<std::size_t, 4> nIntersectionType= {};
    for (const auto& element : elements(gv))
      for (const auto& intersection : intersections(gv, element))
        nIntersectionType[1 * intersection.neighbor() + 2 * intersection.boundary()]++;
    return nIntersectionType;
  }

  void operator()(std::shared_ptr<Grid> grid) const{
    const auto nIntersectionTypeBefore = count_intersection_type(grid->leafGridView());

    std::shared_ptr<Grid> newGrid(Dune::createConsistentGrid(grid));

    const auto nIntersectionTypeAfter = count_intersection_type(newGrid->leafGridView());

    for (std::size_t i = 0; i < nIntersectionTypeBefore.size(); ++i)
      if(nIntersectionTypeAfter[i] != nIntersectionTypeBefore[i])
        DUNE_THROW(Dune::InvalidStateException, "Grid reorder created intersection with different type.");
  }
};

template<typename Grid>
void TestGridReorderFail(std::integral_constant<int, 2>) {}
template<typename Grid>
void TestGridReorderFail(std::integral_constant<int, 3>) {
  auto testFail = [] (std::unique_ptr<Grid> unique_grid){
    std::shared_ptr<Grid> grid(unique_grid.release());
    try {
      std::shared_ptr<Grid> newGrid(Dune::createConsistentGrid(grid));
      DUNE_THROW(Dune::InvalidStateException, "Found a consistent orientation for a non orientable grid");
    } catch (const Dune::NonOrientable &e) {
      grid->globalRefine(1);
      TestGridConsistentAfterReorder<Grid> test;
      test(grid);
    }
  };
  if(Grid::dimension == Grid::dimensionworld)
    testFail(createTwistedTorus<Grid>());
  else
    testFail(createMoebiusBand<Grid>());
}

template<typename Grid>
struct TestConsistentAferWriteRead{
  using Traits = TestTraits<Grid>;

  void operator()(std::shared_ptr<Grid> grid) const{
    Dune::createConsistentGmshFile(grid->leafGridView(), "reordered-grid.msh.tmp");
    grid.reset();
    grid = std::shared_ptr<Grid>(Dune::GmshReader<Grid>::read("reordered-grid.msh.tmp", false, false));

    if(!Dune::hasConsistentOrientation(grid->leafGridView()))
      DUNE_THROW(Dune::InvalidStateException, "Consistent grid is not consistent after reading form gmsh");
  }
};


template<typename Test>
void executeTestOnAllGrids(const Test test){
  using Grid = typename Test::Traits::Grid;
  constexpr int d = Grid::dimension;
  constexpr int world_dim = Grid::dimensionworld;

  static_assert((d == 2 or d == 3), "Dimension must be either 2 or 3");

  if(d == 2) {
    if(world_dim == 2) {
      test(std::shared_ptr<Grid>(Dune::GmshReader<Grid>::read("mesh/square.msh", false, false)));
      test(std::shared_ptr<Grid>(Dune::GmshReader<Grid>::read("mesh/diamond.msh", false, false)));
      test(std::shared_ptr<Grid>(Dune::GmshReader<Grid>::read("mesh/unstructured.msh", false, false)));
    } else{
      Dune::DGFGridFactory<Grid> dgf("mesh/cube-testgrid-2-3.dgf");
      test(std::shared_ptr<Grid>(dgf.grid()));
    }
  } else {
    test(std::shared_ptr<Grid>(Dune::GmshReader<Grid>::read("mesh/cube.msh", false, false)));
    test(std::shared_ptr<Grid>(Dune::GmshReader<Grid>::read("mesh/missingcorner3d.msh", false, false)));
    test(std::shared_ptr<Grid>(Dune::GmshReader<Grid>::read("mesh/cube_hexa_2.msh", false, false)));
    {
      Dune::DGFGridFactory<Grid> dgf("mesh/examplegrid11.dgf");
      test(std::shared_ptr<Grid>(dgf.grid()));
    }
  }
}

template<typename Grid>
void executeAllTests(){
  executeTestOnAllGrids(TestEdgeToAdjacentElementsMap<Grid>());
  executeTestOnAllGrids(TestConsistentOrientationAfterFlip<Grid>());
  executeTestOnAllGrids(TestGridConsistentAfterReorder<Grid>());
  executeTestOnAllGrids(TestGridIntersectionTypeAfterReorder<Grid>());
  executeTestOnAllGrids(TestConsistentAferWriteRead<Grid>());
  TestGridReorderFail<Grid>(std::integral_constant<int, Grid::dimensionworld>{});
}

int main(int argc, char **argv){
  Dune::MPIHelper::instance(argc, argv);

#ifdef HAVE_DUNE_UGGRID
  using UG = Dune::UGGrid<2>;
  executeAllTests<UG>();

  using UG3d = Dune::UGGrid<3>;
  executeAllTests<UG3d>();
#endif

#ifdef HAVE_DUNE_ALUGRID
  using ALU = Dune::ALUGrid<2,2,Dune::cube,Dune::nonconforming,Dune::ALUGridMPIComm>;
  executeAllTests<ALU>();

  using ALU3d = Dune::ALUGrid<3,3,Dune::cube,Dune::nonconforming,Dune::ALUGridMPIComm>;
  executeAllTests<ALU3d>();

//  using ALU2dManifold = Dune::ALUGrid<2,3,Dune::cube,Dune::nonconforming,Dune::ALUGridMPIComm>;
//  executeAllTests<ALU2dManifold>();
#endif
}