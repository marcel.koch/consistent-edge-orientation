// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_GRID_REORDER_TEST_TORUSGRID_HH
#define DUNE_GRID_REORDER_TEST_TORUSGRID_HH

#include <cmath>
#include <dune/common/fmatrix.hh>
#include <dune/grid/common/gridfactory.hh>

template<typename V>
V rotateZ(const V& v, const double alpha){
  Dune::FieldMatrix<double,3,3> R({{std::cos(alpha), -std::sin(alpha), 0},
                                   {std::sin(alpha), std::cos(alpha), 0},
                                   {0, 0, 1}});
  return Dune::FMatrixHelp::mult(R, v);
}

template<typename V>
V rotateX(const V& v, const double alpha){
  Dune::FieldMatrix<double,3,3> R({{1, 0, 0},
                                   {0, std::cos(alpha), -std::sin(alpha)},
                                   {0, std::sin(alpha), std::cos(alpha)}});
  return Dune::FMatrixHelp::mult(R, v);
}

template<typename Grid>
std::unique_ptr<Grid> createTwistedTorus(const unsigned int segments = 6){
  constexpr double pi = M_PI;

  Dune::GridFactory<Grid> gf;

  Dune::FieldVector<double, 3> origin = {0, -1.5, 0.5};
  std::array<Dune::FieldVector<double,3>, 4> faces = {{{0, -2, 0},{0, -1, 0},{0, -2, 1},{0, -1, 1}}};

  for (unsigned int n = 0; n < segments; ++n)
    for (const auto& face : faces)
      gf.insertVertex(rotateZ(rotateX(face - origin, n*(pi/segments)) + origin, n*(2*pi/segments)));

  for (unsigned int n = 0; n < segments - 1; ++n)
    gf.insertElement(Dune::GeometryTypes::cube(3), {4 * n + 0, 4  *n + 1, 4 * n + 2, 4 * n + 3,
                                                    4 * n + 4, 4 * n + 5, 4 * n + 6, 4 * n + 7});
  gf.insertElement(Dune::GeometryTypes::cube(3), {4 * segments - 1, 4 * segments - 2, 4 * segments - 3, 4 * segments - 4,
                                                  0, 1, 2, 3});

  return gf.createGrid();
}


template<typename Grid>
std::unique_ptr<Grid> createMoebiusBand(const unsigned int segments = 6){
  constexpr double pi = M_PI;

  Dune::GridFactory<Grid> gf;

  Dune::FieldVector<double, 3> origin = {0, -1, 0.5};
  std::array<Dune::FieldVector<double,3>, 2> edge_vertices = {{{0, -1, 0},{0, -1, 1}}};

  for (unsigned int n = 0; n < segments; ++n)
    for (const auto& vertex : edge_vertices)
      gf.insertVertex(rotateZ(rotateX(vertex - origin, n*(pi/segments)) + origin, n*(2*pi/segments)));

  for (unsigned int n = 0; n < segments - 1; ++n)
    gf.insertElement(Dune::GeometryTypes::cube(2), {2 * n + 0, 2 * n + 1, 2 * n + 2, 2 * n + 3});
  gf.insertElement(Dune::GeometryTypes::cube(2), {2 * segments - 1, 2 * segments - 2, 0, 1});

  return gf.createGrid();
}

#endif //DUNE_GRID_REORDER_TEST_TORUSGRID_HH
