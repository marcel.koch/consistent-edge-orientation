// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_CONSISTENT_EDGE_ORIENTATION_CREATE_CONSISTENT_GRID_UG_HH
#define DUNE_CONSISTENT_EDGE_ORIENTATION_CREATE_CONSISTENT_GRID_UG_HH

#include <dune/grid/uggrid.hh>

namespace Dune {

  template<int d>
  struct ReorderImplTraits;

  template<>
  struct ReorderImplTraits<2> {
    using BNDS = UG::D2::BNDS;
    static constexpr auto &n_offset = UG::D2::n_offset;
    static constexpr auto &nb_offset = UG::D2::nb_offset;
    static constexpr auto &side_offset = UG::D2::side_offset;
    static constexpr auto &svector_offset = UG::D2::svector_offset;
  };

  template<>
  struct ReorderImplTraits<3> {
    using BNDS = UG::D3::BNDS;
    static constexpr auto &n_offset = UG::D3::n_offset;
    static constexpr auto &nb_offset = UG::D3::nb_offset;
    static constexpr auto &side_offset = UG::D3::side_offset;
    static constexpr auto &svector_offset = UG::D3::svector_offset;
  };

  /**
   * @brief Implements functions to swap certain members of a UG element.
   */
  template<int d>
  struct ReorderImpl {
    using T = ReorderImplTraits<d>;

    template<typename T>
    static void swap_generic(typename Dune::UG_NS<d>::Element *theElement, const int *offset, int a, int b) {
      using UG::UINT;

      auto *temp = (T *) theElement->ge.refs[offset[Dune::UG_NS<d>::Tag(theElement)] + (a)];
      theElement->ge.refs[offset[Dune::UG_NS<d>::Tag(theElement)] + (a)] =
          theElement->ge.refs[offset[Dune::UG_NS<d>::Tag(theElement)] + (b)];
      theElement->ge.refs[offset[Dune::UG_NS<d>::Tag(theElement)] + (b)] = temp;
    }

    static void swap_node(typename Dune::UG_NS<d>::Element *theElement, int n_a, int n_b) {
      swap_generic<typename Dune::UG_NS<d>::Node>(theElement, T::n_offset, n_a, n_b);
    }

    static void swap_neighbor(typename Dune::UG_NS<d>::Element *theElement, int nb_a, int nb_b) {
      swap_generic<typename Dune::UG_NS<d>::Element>(theElement, T::nb_offset, nb_a, nb_b);
    }

    static void swap_bnds(typename Dune::UG_NS<d>::Element *theElement, int bnds_a, int bnds_b) {
      swap_generic<typename T::BNDS>(theElement, T::side_offset, bnds_a, bnds_b);
    }

    static void swap_sidevector(typename Dune::UG_NS<d>::Element *theElement, int vector_a, int vector_b) {
      swap_generic<typename Dune::UG_NS<d>::Vector>(theElement, T::svector_offset, vector_a, vector_b);
    }
  };

  /**
   * @brief Specialization for Dune::UGGrid<2>.
   * For Dune::UGGrid it is faster to manipulate the grid directly, instead of using a Dune::GridFactory to create a
   * new grid.
   */
  template<>
  std::shared_ptr<Dune::UGGrid<2>> createConsistentGrid<Dune::UGGrid<2>>(std::shared_ptr<Dune::UGGrid<2>> grid) {
    auto gv = grid->leafGridView();

    if(Dune::hasConsistentOrientation(gv)){
      return grid;
    } else {
      constexpr std::array<bool, 2> noFlips = {false, false};
      ReorderImpl<2> ops;

      auto verticesDUNEtoUG = [](const int i) {
        return Dune::UGGridRenumberer<2>::verticesDUNEtoUG(i, Dune::GeometryTypes::hexahedron);
      };

      auto edgesDUNEtoUG = [](const int i) {
        return Dune::UGGridRenumberer<2>::edgesDUNEtoUG(i, Dune::GeometryTypes::hexahedron);
      };

      // Compute the new orientation for each edge
      const auto globalOrientation = computeConsistentEdgeOrientation(gv);

      // for each element check if edges parallel to an axis need to be flipped
      // and directly modify the element accordingly
      for (auto &element: Dune::elements(gv)) {
        auto &realElement = element.impl();
        auto *target = realElement.getTarget();

        const auto flip = axisNeedsFlip(gv, element, globalOrientation);

        if (flip[EdgeAxis::x]) {
          ops.swap_node(target, verticesDUNEtoUG(0), verticesDUNEtoUG(1));
          ops.swap_node(target, verticesDUNEtoUG(2), verticesDUNEtoUG(3));

          ops.swap_neighbor(target, edgesDUNEtoUG(0), edgesDUNEtoUG(1));
          if (Dune::UG_NS<2>::isBoundaryElement(target))
            ops.swap_bnds(target, edgesDUNEtoUG(0), edgesDUNEtoUG(1));
        }
        if (flip[EdgeAxis::y]) {
          ops.swap_node(target, verticesDUNEtoUG(0), verticesDUNEtoUG(2));
          ops.swap_node(target, verticesDUNEtoUG(1), verticesDUNEtoUG(3));

          ops.swap_neighbor(target, edgesDUNEtoUG(2), edgesDUNEtoUG(3));
          if (Dune::UG_NS<2>::isBoundaryElement(target))
            ops.swap_bnds(target, edgesDUNEtoUG(2), edgesDUNEtoUG(3));
        }

        if (impl::negativeVolume(element, noFlips, std::integral_constant<std::size_t, 2>{})) {
          ops.swap_node(target, verticesDUNEtoUG(1), verticesDUNEtoUG(2));
          ops.swap_neighbor(target, edgesDUNEtoUG(0), edgesDUNEtoUG(2));
          ops.swap_neighbor(target, edgesDUNEtoUG(1), edgesDUNEtoUG(3));
          if (Dune::UG_NS<2>::isBoundaryElement(target)) {
            ops.swap_bnds(target, edgesDUNEtoUG(0), edgesDUNEtoUG(2));
            ops.swap_bnds(target, edgesDUNEtoUG(1), edgesDUNEtoUG(3));
          }
        }
      }

      return grid;
    }
  }
  /**
   * @brief Specialization for Dune::UGGrid<3>.
   * For Dune::UGGrid it is faster to manipulate the grid directly, instead of using a Dune::GridFactory to create a
   * new grid.
   */
  template<>
  std::shared_ptr<Dune::UGGrid<3>> createConsistentGrid<Dune::UGGrid<3>>(std::shared_ptr<Dune::UGGrid<3>> grid)
  {
    auto gv = grid->leafGridView();

    if(Dune::hasConsistentOrientation(gv)){
      return grid;
    } else {
      constexpr std::array<bool, 3> noFlips = {false, false, false};
      ReorderImpl<3> ops;

      auto verticesDUNEtoUG = [](const int i) {
        return Dune::UGGridRenumberer<3>::verticesDUNEtoUG(i, Dune::GeometryTypes::hexahedron);
      };

      auto facesDUNEtoUG = [](const int i) {
        return Dune::UGGridRenumberer<3>::facesDUNEtoUG(i, Dune::GeometryTypes::hexahedron);
      };

      // Compute the new orientation for each edge
      const auto globalOrientation = computeConsistentEdgeOrientation(gv);

      // for each element check if edges parallel to an axis need to be flipped
      // and directly modify the element accordingly
      for (auto &element: Dune::elements(gv)) {
        auto &realElement = element.impl();
        auto *target = realElement.getTarget();

        const auto flip = axisNeedsFlip(gv, element, globalOrientation);

        if (flip[EdgeAxis::x]) {
          ops.swap_node(target, verticesDUNEtoUG(0), verticesDUNEtoUG(1));
          ops.swap_node(target, verticesDUNEtoUG(2), verticesDUNEtoUG(3));
          ops.swap_node(target, verticesDUNEtoUG(4), verticesDUNEtoUG(5));
          ops.swap_node(target, verticesDUNEtoUG(6), verticesDUNEtoUG(7));

          ops.swap_neighbor(target, facesDUNEtoUG(0), facesDUNEtoUG(1));
          ops.swap_sidevector(target, facesDUNEtoUG(0), facesDUNEtoUG(1));
          if (Dune::UG_NS<3>::isBoundaryElement(target))
            ops.swap_bnds(target, facesDUNEtoUG(0), facesDUNEtoUG(1));
        }
        if (flip[EdgeAxis::y]) {
          ops.swap_node(target, verticesDUNEtoUG(0), verticesDUNEtoUG(2));
          ops.swap_node(target, verticesDUNEtoUG(1), verticesDUNEtoUG(3));
          ops.swap_node(target, verticesDUNEtoUG(4), verticesDUNEtoUG(6));
          ops.swap_node(target, verticesDUNEtoUG(5), verticesDUNEtoUG(7));

          ops.swap_neighbor(target, facesDUNEtoUG(2), facesDUNEtoUG(3));
          ops.swap_sidevector(target, facesDUNEtoUG(2), facesDUNEtoUG(3));
          if (Dune::UG_NS<3>::isBoundaryElement(target))
            ops.swap_bnds(target, facesDUNEtoUG(2), facesDUNEtoUG(3));
        }
        if (flip[EdgeAxis::z]) {
          ops.swap_node(target, verticesDUNEtoUG(0), verticesDUNEtoUG(4));
          ops.swap_node(target, verticesDUNEtoUG(1), verticesDUNEtoUG(5));
          ops.swap_node(target, verticesDUNEtoUG(2), verticesDUNEtoUG(6));
          ops.swap_node(target, verticesDUNEtoUG(3), verticesDUNEtoUG(7));

          ops.swap_neighbor(target, facesDUNEtoUG(4), facesDUNEtoUG(5));
          ops.swap_sidevector(target, facesDUNEtoUG(4), facesDUNEtoUG(5));
          if (Dune::UG_NS<3>::isBoundaryElement(target))
            ops.swap_bnds(target, facesDUNEtoUG(4), facesDUNEtoUG(5));
        }

        if (impl::negativeVolume(element, noFlips, std::integral_constant<std::size_t, 3>{})) {
          ops.swap_node(target, verticesDUNEtoUG(1), verticesDUNEtoUG(2));
          ops.swap_node(target, verticesDUNEtoUG(5), verticesDUNEtoUG(6));

          ops.swap_neighbor(target, facesDUNEtoUG(0), facesDUNEtoUG(2));
          ops.swap_neighbor(target, facesDUNEtoUG(1), facesDUNEtoUG(3));
          ops.swap_sidevector(target, facesDUNEtoUG(0), facesDUNEtoUG(2));
          ops.swap_sidevector(target, facesDUNEtoUG(1), facesDUNEtoUG(3));
          if (Dune::UG_NS<3>::isBoundaryElement(target)) {
            ops.swap_bnds(target, facesDUNEtoUG(0), facesDUNEtoUG(2));
            ops.swap_bnds(target, facesDUNEtoUG(1), facesDUNEtoUG(3));
          }
        }

      }

      return grid;
    }
  }
}
#endif //DUNE_CONSISTENT_EDGE_ORIENTATION_CREATE_CONSISTENT_GRID_UG_HH
