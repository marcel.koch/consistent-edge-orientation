// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_CONSISTENT_EDGE_ORIENTATION_CREATE_CONSISTENT_GRID_HH
#define DUNE_CONSISTENT_EDGE_ORIENTATION_CREATE_CONSISTENT_GRID_HH

#include <cfloat>

#include <dune/common/power.hh>
#include <dune/grid/common/gridfactory.hh>
#include <dune/grid/io/file/gmshwriter.hh>
#include "paralleledges.hh"

namespace Dune {
  namespace impl{
    template<typename F>
    void applySwapIndices(const std::array<bool, 2> &flip, F &swapIndices, std::integral_constant<int, 2>){
      if (flip[EdgeAxis::x])
        swapIndices({{{0, 1}, {2, 3}}});
      if (flip[EdgeAxis::y])
        swapIndices({{{0, 2}, {1, 3}}});
    }
    template<typename F>
    void applySwapIndices(const std::array<bool, 3> &flip, F& swapIndices, std::integral_constant<int, 3>){
      if (flip[EdgeAxis::x])
        swapIndices({{{0, 1}, {2, 3}, {4, 5}, {6, 7}}});
      if (flip[EdgeAxis::y])
        swapIndices({{{0, 2}, {1, 3}, {4, 6}, {5, 7}}});
      if (flip[EdgeAxis::z])
        swapIndices({{{0, 4}, {1, 5}, {2, 6}, {3, 7}}});
    }

    template<std::size_t d>
    std::array<std::size_t, Dune::StaticPower<2, d>::power> permuteVertices(const std::array<bool, d> &flip) {
      std::array<std::size_t, Dune::StaticPower<2, d>::power> indices = {};
      for (std::size_t i = 0; i < indices.size(); ++i)
        indices[i] = i;

      auto swapIndices =
          [&indices](const std::array<std::array<unsigned int, 2>, Dune::StaticPower<2, d - 1>::power> &permutations) {
            for (const auto &permutation: permutations)
              std::swap(indices[permutation[0]], indices[permutation[1]]);
          };

      applySwapIndices(flip, swapIndices, std::integral_constant<int, d>{});

      return indices;
    }

    template<typename Element, std::size_t d, std::size_t world_dim>
    bool negativeVolume(const Element& e, const std::array<bool, d>& flip,
                        std::integral_constant<std::size_t, world_dim>){
      if(d != world_dim) return false;

      const auto permutation = permuteVertices(flip);
      Dune::FieldVector<double, world_dim> origin = e.geometry().corner(permutation[0]);
      std::array<Dune::FieldVector<double, world_dim>, d> vertices = {};

      vertices[0] = e.geometry().corner(permutation[1]);
      vertices[1] = e.geometry().corner(permutation[2]);
      if(d == 3)
        vertices[2] = e.geometry().corner(permutation[4]);

      for(auto& v: vertices)
        v -= origin;

      // use the same check as in UG to prevent UG from reordering the element corners
      if(d == 2) {
        return (vertices[0][0]*vertices[1][1] - vertices[1][0]*vertices[0][1]) < FLT_EPSILON*10;
      } else {
        Dune::FieldVector<double, d> n = {vertices[0][1] * vertices[1][2] - vertices[1][1] * vertices[0][2],
                                          vertices[0][2] * vertices[1][0] - vertices[1][2] * vertices[0][0],
                                          vertices[0][0] * vertices[1][1] - vertices[1][0] * vertices[0][1]};
        return n.dot(vertices[2]) < FLT_EPSILON*10;
      }
    }

    template<std::size_t n>
    std::array<std::size_t, n> invertPermutation(const std::array<std::size_t, n>& permutation){
      std::array<std::size_t, n> inverse = {};
      for (std::size_t i = 0; i < n; ++i)
        for (std::size_t j = 0; j < n; ++j)
          if( permutation[j] == i) {
            inverse[i] = j;
            break;
          }

      return inverse;
    }

    std::array<int, 2> intersectionLocalCorners(int isecIndex, std::integral_constant<int, 2>) {
      static constexpr std::array<int, 2> corners[4] = {{0, 2}, {1, 3},
                                                        {0, 1}, {2, 3}};
      return corners[isecIndex];
    }
    std::array<int, 4> intersectionLocalCorners(int isecIndex, std::integral_constant<int, 3>) {
      static constexpr std::array<int, 4> corners[6] = {{0, 2, 4, 6}, {1, 3, 5, 7}, {0, 1, 4, 5},
                                                        {2, 3, 6, 7}, {0, 1, 2, 3}, {4, 5, 6, 7}};
      return corners[isecIndex];
    }
  }

  /**
 * @brief Given a grid view, this function creates a Gmsh file, which describes a consitently oriented mesh.
 * @tparam GV DUNE grid view type. The corresponding grid type needs to preserves the edge orientation in elements
 *            with positive volume, given by a Gmsh file.
 * @param gv grid view (currently only tested with leaf grid view)
 */
  template<typename GV>
  void createConsistentGmshFile(const GV& gv, const std::string& fileName) {
    if(Dune::hasConsistentOrientation(gv)){
      Dune::GmshWriter<GV> gmshWriter(gv);

      gmshWriter.write(fileName);
    } else {
      constexpr int dim = GV::dimension;
      constexpr int world_dim = GV::dimensionworld;

      // Open file
      std::ofstream file(fileName.c_str());
      if (!file.is_open())
        DUNE_THROW(Dune::IOError, "Could not open " << fileName << " with write access.");

      // Set precision
      file << std::setprecision(6);

      // Output Header
      file << "$MeshFormat" << std::endl
           << "2.0 0 " << sizeof(double) << std::endl // "2.0" for "version 2.0", "0" for ASCII
           << "$EndMeshFormat" << std::endl;

      // Output Nodes
      file << "$Nodes" << std::endl
           << gv.size(dim) << std::endl;

      for (const auto &vertex : vertices(gv)) {
        const auto globalCoord = vertex.geometry().center();
        const auto nodeIndex = gv.indexSet().index(vertex) + 1; // Start counting indices by "1".

        if (2 == dim)
          file << nodeIndex << " " << globalCoord[0] << " " << globalCoord[1] << " " << 0 << std::endl;
        else
          file << nodeIndex << " " << globalCoord[0] << " " << globalCoord[1] << " " << globalCoord[2] << std::endl;
      }

      file << "$EndNodes" << std::endl
           << "$Elements" << std::endl
           << gv.size(0) << std::endl;

      const auto &idxSet = gv.indexSet();
      // compute global index of corner (+ 1 for gmsh)
      auto gmshNodeIndex = [dim, &idxSet](const auto &e, const int i) { return idxSet.subIndex(e, i, dim) + 1; };

      try {
        // Compute the new orientation for each edge
        const auto globalOrientation = computeConsistentEdgeOrientation(gv);

        std::size_t counter(1);
        for (const auto &element : elements(gv)) {

          // for each element check if edges parallel to an axis need to be flipped and
          // permutate the vertices s.t. the element has positive volume
          const auto flip = axisNeedsFlip(gv, element, globalOrientation);
          auto permutation = impl::permuteVertices(flip);
          if (impl::negativeVolume(element, flip, std::integral_constant<std::size_t, world_dim>{})) {
            std::swap(permutation[1], permutation[2]);
            if (dim == 3)
              std::swap(permutation[5], permutation[6]);
          }

          std::size_t element_type = dim == 2 ? 3 : 5;

          // currently physical entities are not supported
          file << counter << " " << element_type << " " << 0;

          // Output list of nodes.
          if (3 == element_type)
            file << " "
                 << gmshNodeIndex(element, permutation[0]) << " " << gmshNodeIndex(element, permutation[1]) << " "
                 << gmshNodeIndex(element, permutation[3]) << " " << gmshNodeIndex(element, permutation[2]);
          else
            file << " "
                 << gmshNodeIndex(element, permutation[0]) << " " << gmshNodeIndex(element, permutation[1]) << " "
                 << gmshNodeIndex(element, permutation[3]) << " " << gmshNodeIndex(element, permutation[2]) << " "
                 << gmshNodeIndex(element, permutation[4]) << " " << gmshNodeIndex(element, permutation[5]) << " "
                 << gmshNodeIndex(element, permutation[7]) << " " << gmshNodeIndex(element, permutation[6]);
          ++counter;

          file << std::endl;
        }
        file << "$EndElements" << std::endl;
      } catch (std::exception &e) {
        file.close();
        throw;
      }
    }
  }

  /**
   * @brief Given a non refined macro mesh, this function creates a new grid, which is consitently oriented.
   * @tparam Grid DUNE grid type, which preserves the edge orientation in element with positive volume, given in a Gmsh
   *              file.
   * @return New grid, which is consitently oriented.
   */
  template<typename Grid>
  std::shared_ptr<Grid> createConsistentGrid(std::shared_ptr<Grid> grid) {
    constexpr int dim = Grid::dimension;
    constexpr int world_dim = Grid::dimensionworld;
    auto gv = grid->leafGridView();

    if(Dune::hasConsistentOrientation(gv)){
      return grid;
    } else {

      const auto &idxSet = gv.indexSet();

      // Compute the new orientation for each edge
      const auto globalOrientation = computeConsistentEdgeOrientation(gv);

      Dune::GridFactory<Grid> gridFactory;

      std::vector<std::size_t> vertexInsertionIndex(gv.size(dim));

      std::size_t index = 0;
      for (const auto &vertex: Dune::vertices(gv)) {
        gridFactory.insertVertex(vertex.geometry().center());
        vertexInsertionIndex[idxSet.index(vertex)] = index++;
      }

      for (const auto &element: Dune::elements(gv)) {
        std::vector<unsigned int> vertices(Dune::StaticPower<2, dim>::power);

        // for each element check if edges parallel to an axis need to be flipped and
        // permutate the vertices s.t. the element has positive volume
        const auto flip = axisNeedsFlip(gv, element, globalOrientation);
        auto permutation = impl::permuteVertices(flip);
        if (impl::negativeVolume(element, flip, std::integral_constant<std::size_t, world_dim>{})) {
          std::swap(permutation[1], permutation[2]);
          if (dim == 3)
            std::swap(permutation[5], permutation[6]);
        }

        for (std::size_t i = 0; i < vertices.size(); ++i)
          vertices[i] = vertexInsertionIndex[idxSet.subIndex(element, permutation[i], dim)];

        gridFactory.insertElement(Dune::GeometryTypes::cube(dim), vertices);

        for (const auto &isec: Dune::intersections(gv, element)) {
          if (isec.boundary()) {
            std::vector<unsigned int> bdryVertices(Dune::StaticPower<2, dim - 1>::power);
            const auto corners = impl::intersectionLocalCorners(isec.indexInInside(),
                                                                std::integral_constant<int, dim>{});
            const auto inversePermutation = impl::invertPermutation(permutation);
            for (std::size_t i = 0; i < bdryVertices.size(); ++i)
              bdryVertices[i] = vertices[inversePermutation[corners[i]]];

            gridFactory.insertBoundarySegment(bdryVertices);
          }
        }
      }

      return std::shared_ptr<Grid>(gridFactory.createGrid());
    }
  }
}

#if HAVE_DUNE_UGGRID
#include "uggrid/createconsistentgrid.hh"
#endif

#endif //DUNE_CONSISTENT_EDGE_ORIENTATION_CREATE_CONSISTENT_GRID_HH
