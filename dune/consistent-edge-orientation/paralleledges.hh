// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_CONSISTENT_EDGE_ORIENTATION_PARALLELEDGES_HH
#define DUNE_CONSISTENT_EDGE_ORIENTATION_PARALLELEDGES_HH

#include <vector>

#include <dune/grid/common/rangegenerators.hh>
#include <dune/common/reservedvector.hh>
#include "utilities.hh"
#include "union_find.hh"

namespace Dune{
  class NonOrientable : public Dune::Exception {};

  /**
   * @brief Computes for each edge, the set of all Dune::EdgeWrapper, which are globally the same edge, but within
   * different elements.
   * @param gv leaf grid view
   * @return Dune::EdgeContainer of vector<EdgeWrapper>.
   */
  template<typename GV>
  AdjacentElementsMap<GV> computeEdgeToAdjacentElementsMap(const GV& gv){
    constexpr int dim = GV::Grid::dimension;
    AdjacentElementsMap<GV> edgeInAdjacentElements(gv);

    for(auto& edges: edgeInAdjacentElements)
      edges.reserve(1 << (dim - 1));

    for (const auto &e: Dune::elements(gv)) {
      for (const auto axis: range(dim)) {
        const auto edges = EdgeWrapper<GV>::generate_axis(gv, e, EdgeAxis(axis));
        for (const auto parallelEdge: edges) {
          edgeInAdjacentElements[parallelEdge].emplace_back(parallelEdge);
        }
      }
    }

    return edgeInAdjacentElements;
  }

  /**
   * @brief Assigns each edge a Dune::EdgeOrientation, s.t. if all edges have the prescribed orientation, the
   * orientation of an edge shared by two or more elements would be consistent.
   * @param gv leaf grid view.
   * @exception Dune::NonOrientable if the mesh is not orientable.
   * @return Dune::EdgeContainer of Dune::EdgeOrientation.
   */
  template<typename GV>
  OrientationMap<GV> computeConsistentEdgeOrientation(const GV& gv){
    EdgeMap<bool, GV> visited(gv);
    OrientationMap<GV> globalOrientation(gv);

    std::fill(globalOrientation.begin(), globalOrientation.end(), EdgeOrientation::positive);
    std::fill(visited.begin(), visited.end(), false);

    const auto edgeInAdjacentElements = computeEdgeToAdjacentElementsMap(gv);

    constexpr auto dim = GV::dimension;
    const auto& idxSet = gv.indexSet();

    // iterate over all edges
    for(const auto& e: elements(gv)){
      for(std::size_t edgeIndex = 0; edgeIndex < e.subEntities(dim - 1); ++edgeIndex){
        EdgeWrapper<GV> firstEdge(e, edgeIndex, idxSet.subIndex(e, edgeIndex, dim - 1));

        if(!visited[firstEdge]) {
          visited[firstEdge] = true;

          std::vector<EdgeWrapper<GV>> stack{firstEdge};

          // iterate over the current edge set
          while (!stack.empty()) {
            const auto nextEdge = stack.back();
            stack.pop_back();

            for (const auto &currentEdge: edgeInAdjacentElements[nextEdge]) {
              const auto parallelEdges = parallelEdgesInElement(gv, currentEdge);

              for (const auto &parallelEdge: parallelEdges) {
                const bool flipped = flippedRelativeOrientation(gv, currentEdge, parallelEdge);

                if (!visited[parallelEdge]) {
                  visited[parallelEdge] = true;

                  // if both edges have the same old orientation, they also have the same new orientation,
                  // otherwise the new orientation of the parallelEdge is the opposite of the currentEdge's orientation
                  if (flipped)
                    globalOrientation[parallelEdge] = flipOrientation(globalOrientation[currentEdge]);
                  else
                    globalOrientation[parallelEdge] = globalOrientation[currentEdge];

                  stack.emplace_back(parallelEdge);
                } else if (flipped xor (globalOrientation[parallelEdge] != globalOrientation[currentEdge]))
                  DUNE_THROW(NonOrientable, "Encountered non orientable surface");
              }
            }
          }
        }
      }
    }

    return globalOrientation;
  }

  /**
   * @brief Decides if the grid is consistently oriented.
   * @param gv leaf grid view.
   */
  template<typename GV>
  bool hasConsistentOrientation(const GV& gv){
    auto adjacentElements = Dune::computeEdgeToAdjacentElementsMap(gv);

    for(const auto& neighbors: adjacentElements){
      const auto positiveOrientations = (std::size_t)
          std::count_if(neighbors.begin(), neighbors.end(),
                        [&gv] (const auto& edgeWrapper){
                          return edgeOrientation(gv, edgeWrapper) == Dune::EdgeOrientation::positive;
                        });
      if(not(positiveOrientations == 0 or positiveOrientations == neighbors.size()))
        return false;
    }
    return true;
  }
}

#endif //DUNE_CONSISTENT_EDGE_ORIENTATION_PARALLELEDGES_HH
