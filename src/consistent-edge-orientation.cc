// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <iostream>

#include <dune/common/exceptions.hh>
#include <dune/common/classname.hh>
#include <dune/grid/io/file/gmshreader.hh>
#include <dune/grid/io/file/gmshwriter.hh>
#include <dune/consistent-edge-orientation/createconsistentgrid.hh>
#if HAVE_UG
#include<dune/grid/uggrid.hh>

using Grid = Dune::UGGrid<GRID_DIM>;
#elif HAVE_DUNE_ALUGRID
#include<dune/alugrid/grid.hh>
#include<dune/alugrid/dgf.hh>
#include<dune/grid/io/file/dgfparser/dgfparser.hh>

using Grid = Dune::ALUGrid<GRID_DIM, GRID_DIM, Dune::cube,Dune::nonconforming,Dune::ALUGridMPIComm>;
#endif

void printHelp(){
  std::cout << "usage: consistent-edge-orientation <input-mesh> [output-mesh] [-f]" << std::endl;
  std::cout << "Options:" << std::endl;
  std::cout << "input-mesh\t: the mesh to be oriented in Gmsh format" << std::endl;
  std::cout << "output-mesh\t: the name of the generated mesh, if none given the name is \"reordered-input-mesh\""
            << std::endl;
  std::cout << "-f\t\t: if set the input mesh will be refined, if an consistent orientation could not be found,"
            << " without additional user confirmation (has no effect in 2d)" << std::endl;
}


int main(int argc, char** argv){
  const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);
  // check that we are not run through mpirun
  if(mpiHelper.size() > 1)
    if(mpiHelper.rank() == 0)
      DUNE_THROW(Dune::NotImplemented, "Parallel algorithm currently not implemented.");

  if(argc < 2 or std::string(argv[1]) == "--help") {
    printHelp();
    return 0;
  }

  std::string outFile;
  if(argc > 2 and not (std::string(argv[2]) == "-f"))
    outFile = std::string(argv[2]);
  else
    outFile = "reordered-"+std::string(argv[1]);

  std::shared_ptr<Grid> grid(Dune::GmshReader<Grid>::read(argv[1]));
  try {
    Dune::createConsistentGmshFile(grid->leafGridView(), outFile);
  } catch(const Dune::NonOrientable){
    if(argc > 2 and std::string(argv[argc - 1]) == "-f")
      grid->globalRefine(1);
    else{
      std::string choice;
      std::cout << "The current grid does not have a consistent edge orientation." << std::endl;
      std::cout << "Should the grid (" << grid->leafGridView().size(0) << " elements) be uniformly refined to obtain "
                << "a consistent edge orientation? [y/n] ";
      std::cin >> choice;
      if(choice == "y" or choice == "Y")
        grid->globalRefine(1);
      else
        return 0;
    }
    Dune::createConsistentGmshFile(grid->leafGridView(), outFile);
  }
}